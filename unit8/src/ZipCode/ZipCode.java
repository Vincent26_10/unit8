package ZipCode;

import java.util.Scanner;
import java.io.*;

public class ZipCode {
	private static String codProv;
	private static String cityOfProvince;
	private static String bigCity;
	private static String zipCodeStr;
	private static String streetOrVillage;

	public static final String FILE_NAME = "codPos.csv";

	// private static String[] objective = null;

	public static void main(String[] args) {
		try {
			System.out.println(getText(12560));
		} catch (IOException ex) {
			System.err.println("ERROR");
			ex.printStackTrace();
		}

	}

	public static String getText(int zipCode) throws IOException {
		BufferedReader in = null;
		try {
			in = new BufferedReader(new FileReader(FILE_NAME));
			String line;

			while ((line = in.readLine()) != null) {
				Scanner scan = new Scanner(line);
				scan.useDelimiter(";");
				 codProv = scan.next();
				 cityOfProvince = scan.next();
				 bigCity = scan.next();
				 zipCodeStr = scan.next();
				 streetOrVillage = scan.next();
				scan.close();

				int zipCodeInt = Integer.parseInt(zipCodeStr);
				if (zipCodeInt > zipCode) {
					break;
				}
				if (zipCode == zipCodeInt) {
					String result = "CodProvince:" + codProv + "\n";

					if (bigCity.equalsIgnoreCase("true")) {
						result += "City: " + cityOfProvince + "\n";
						result += "First Street:" + streetOrVillage + "\n";
					} else {
						result += "Province: " + cityOfProvince + "\n";
						result += "City: " + streetOrVillage + "\n";
					}
					result += "Zip Code; " + zipCodeStr;
					return result;

				}
			}
			return "Notfound";
		} finally {
			if (in != null) {
				in.close();
			}

		}
	}

}
