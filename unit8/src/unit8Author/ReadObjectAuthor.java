package unit8Author;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class ReadObjectAuthor {

	public static void main(String[] args) throws IOException {
		ObjectInputStream input = null;
		
		try {
			input = new ObjectInputStream(new BufferedInputStream(new FileInputStream(StoreObjectAuthor.DATA_FILE)));
			
			
			try {
				while(true) {
					Author a = (Author) input.readObject();
					
					System.out.println(a);
				}
			}catch(ClassNotFoundException ex) {
				System.out.println();
			}
		}finally {
			if(input != null) {
				input.close();
			}
		}
	}

}
