package unit8Author;

import java.io.Serializable;
import java.util.Scanner;

public class Book implements Serializable {

	private static final long serialVersionUID = 1L;
	
	
	private String title;
	private Author author;
	private double price;
	private int qtyInStock;

	public Book(String title, Author author, double price) {
		this.title = title;
		this.author = author;
		this.price = price;
		qtyInStock = 0;

	}

	public Book(String title, Author author, double price, int qtyInStock) {
		this.title = title;
		this.author = author;
		this.price = price;
		this.qtyInStock = qtyInStock;

	}
	
	public static Book BookFormKeyboard(Scanner input) {
		String title;
		double price;
		int qtyInStock;
		
		System.out.println("Title name?");
		title = input.nextLine();
		System.out.println("Price?");
		price = input.nextDouble();
		System.out.println("Quantity?");
		qtyInStock=input.nextInt();
		Author a = Author.createAuthorFormKeyboard(input);
		
		Book b = new Book(title,a, price,qtyInStock);
		return b;
	}

	public String getTitle() {
		return title;
	}

	public Author getAuthor() {
		return author;
	}

	public double getPrice() {
		return price;
	}

	public int getQtyInStock() {
		return qtyInStock;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public void setQtyInStock(int qtyInStock) {
		this.qtyInStock = qtyInStock;
	}
	
	@Override
	public String toString() {
		return title+" by " + author.toString();
	}

}
