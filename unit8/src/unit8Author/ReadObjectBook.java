package unit8Author;

import java.io.*;

public class ReadObjectBook {

	public static void main(String[] args) throws IOException {
		ObjectInputStream input = null;

		try {
			input = new ObjectInputStream(new BufferedInputStream(new FileInputStream(StoreObjectAuthor.DATA_FILE)));

			try {
				while (true) {
					Book b = (Book) input.readObject();
					
					System.out.println(b);
				}
			} catch (ClassNotFoundException ex) {
				System.out.println();
			}
		} finally {
			if (input != null) {
				input.close();
			}
		}
	}
}
