package unit8Author;

import java.io.*;
import java.util.Scanner;

public class StoreObjectBook {
	public static final String FILE_NAME="Books.obj";

	public static void main(String[] args) throws IOException {
		ObjectOutputStream out=null;
		Scanner input = new Scanner(System.in);
		
		try {
			out= new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(FILE_NAME)));
			
			Book b = Book.BookFormKeyboard(input);
			
			out.writeObject(b);
		}finally {
			if(out!=null) {
				out.close();
			}
		}
	}

}
