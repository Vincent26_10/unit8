package unit8Author;

import java.io.*;
import java.util.Scanner;

public class StoreObjectAuthor  {
	public static final String DATA_FILE =  "Authors.obj";


	public static void main(String[] args) throws IOException {
		ObjectOutputStream out = null;
		Scanner input = new Scanner(System.in);
		
		try {
			out = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(DATA_FILE)));
			
			Author a = Author.createAuthorFormKeyboard(input);
			
			out.writeObject(a);
			
		}finally {
			if(out!= null) {
				out.close();
			}
		}
	}
	
		
	
}
