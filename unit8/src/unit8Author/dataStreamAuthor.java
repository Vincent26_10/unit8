package unit8Author;

import java.io.*;
import java.util.*;

public class dataStreamAuthor {
	private static String dataFile =  "authorsFile";
	
	
	public static void main(String[] args) throws IOException {
		//writeAuthor();
		readAuthor();
	}

	public static void writeAuthor() throws IOException {
		Scanner input = new Scanner(System.in);
		DataOutputStream out = null;
		try {
			out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(dataFile)));
			Author a  = Author.createAuthorFormKeyboard(input);
			input.close();
			
			out.writeUTF(a.getName());
			out.writeUTF(a.getEmail());
			out.writeChar(a.getGender());
			
			
		}finally {
			if(out != null) {
				out.close();
			}
		}
	} 
	
	public static void readAuthor() throws IOException {
		DataInputStream input = null;
		try {
			input = new DataInputStream(new BufferedInputStream(new FileInputStream(dataFile)));
			try {
				while(true) {
					String name = input.readUTF();
					String email = input.readUTF();
					char gender = input.readChar();
					
					System.out.println("The author's name is "+ name + "\n");
					System.out.println("The author's email is "+ email + "\n");
					System.out.println("The author's gender is "+ gender + "\n");
				}
			}catch(IOException ex) {
				System.out.println();
			}
		}finally {
			if(input != null) {
				input.close();
			}
				
			}
	}
	
		
}
