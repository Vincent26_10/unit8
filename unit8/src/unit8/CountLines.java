package unit8;
//
import java.io.*;

public class CountLines {

	public static void main(String[] args) throws IOException {
		
		if(args.length != 1) {
			System.err.println("Usage: CountLines <file1>");
		}
		
		
		BufferedReader input = null;
		int count=0;
		try {
			input = new BufferedReader(new FileReader(args[0]));
			
			while(input.readLine()!= null) {
				count++;
			}
		}finally {
			if(input!= null) {
				input.close();
			}
			System.out.println("The number of lines are:" + count);
		}
	}

}
