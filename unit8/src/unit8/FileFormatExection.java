package unit8;

public class FileFormatExection extends Exception {

	public FileFormatExection() {
		super();
	}
	
	public FileFormatExection(String msg) {
		super(msg);
	}
}
