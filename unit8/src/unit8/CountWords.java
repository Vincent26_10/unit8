package unit8;

import java.io.*;
import java.util.Scanner;
//
public class CountWords {
	public static void main(String[] args) throws IOException {
		
		int totalCount=0;
		checkNumArguments(args);
		for(String filename: args) {
			totalCount+= numWordsInFile(filename);
		}
		
		System.out.println("The total number of words is "+totalCount);

	}

	public static int numWordsInFile(String filename) throws FileNotFoundException {
		Scanner s = null;

		
		try {

			int count=0;
			
				s = new Scanner(new BufferedReader(new FileReader(filename)));
			
				while (s.hasNext()) {
					count++;
					s.next();
				}
			
			
			System.out.println(filename+" has: " + count);
			return count;
		} finally {
			if (s != null) {
				s.close();
				
			}
			
		}
	}

	public static void checkNumArguments(String[] args) {
		if(args.length < 1) {
			System.err.println("Usage: CountWords <file1>");
			System.exit(1);
		}
	}
}
