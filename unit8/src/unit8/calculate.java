package unit8;
//
import java.io.*;
import java.util.Scanner;

public class calculate {

	public static void main(String[] args) throws IOException {
		calculateCode();
	}

	public static void calculateCode() throws FileNotFoundException, IOException {
		BufferedReader in = null;

		try {
			in = new BufferedReader(new FileReader("test.csv"));
			String s = null;
			Scanner s2 = null;
			Scanner s3 = null;
			String[] names=null;
			int fields = 0;

			if ((s = in.readLine()) != null) {
				try {
					s2 = new Scanner(s);
					s2.useDelimiter(",");
					while (s2.hasNext()) {
						fields++;
						s2.next();
					}
					s3 = new Scanner(s);
					s3.useDelimiter(",");
					names= new String[fields];
					for(int i=0;i<names.length;i++) {
						names[i]=s3.next();
					}
				} finally {
					s2.close();
				}
			}

			int[] fieldmatrix = new int[fields];

			for (int i = 0; i < fieldmatrix.length; i++) {
				fieldmatrix[i] = 0;
			}

			while ((s = in.readLine()) != null) {
				try {
					s2 = new Scanner(s);
					s2.useDelimiter(",");
					int field = 0;
					while (s2.hasNext()) {
						fieldmatrix[field] += s2.nextInt();
						field++;
					}
				} finally {
					s2.close();
				}
				
			}
			for (int i = 0; i < fieldmatrix.length; i++) {
				System.out.print(names[i]+": ");
				System.out.println(fieldmatrix[i]);
			}
		} finally {
			if (in != null) {
				in.close();

			}
		}
	}

}
