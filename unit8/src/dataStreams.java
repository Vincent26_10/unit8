import java.io.*;

public class dataStreams {
	
	static final String dataFile = "invoicedata";
	static final double[] prices = { 19.99, 9.99, 15.99, 3.99, 4.99 };
	static final int[] units = { 12, 8, 13, 29, 50 };
	static final String[] descs = { "Java T-shirt", "Java Mug", "Duke Juggling Dolls", "Java Pin", "Java Key Chain" };

	public static void main(String[] args)  throws IOException {
		writerFile();
		readFile();
	}
	
	public static void writerFile() throws IOException {
		DataOutputStream out = null;
		try {
			out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(dataFile)));
			for(int i=0;i<prices.length;i++) {
				out.writeDouble(prices[i]);
				out.writeInt(units[i]);
				out.writeUTF(descs[i]);
			}
		}finally {
			if(out !=null) {
				out.close();
			}
		}
	
		
	}
	
	public static void readFile() throws IOException {
		DataInputStream input = null;
		try {
			input = new DataInputStream(new BufferedInputStream(new FileInputStream(dataFile)));
			try {
				while(true) {
				double price = input.readDouble();
				int units = input.readInt();
				String desc = input.readUTF();
				
				System.out.print(desc + " . ");
				System.out.print("Price: " + price);
				System.out.print(". Units: "+ units);
				System.out.print(". TOTAL: "+ price * units);
				System.out.println();
				}
			}catch (IOException ex) {
				System.out.println();
			}
		}finally {
				if(input != null) {
					input.close();
				}
			}
		}
	}


